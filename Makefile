
dump = @echo $0:$1 = $($1)

C = $(wildcard *.c)
CPP = $(wildcard *.cpp)

T = $(C:.c=)
T += $(CPP:.cpp=)

PDF = $(C:.c=.pdf)
EXE = $(addsuffix .exe,$T)
BINDIR = $(ROOT)/usr/bin


.PHONY: all install

CFLAGS = -g -Wall 
CFLAGS += -fopenmp 
LDLIBS = -larchive


%.pdf : %.c
	a2ps -o /dev/stdout $< | ps2pdf - - > $@

all: $T 

pdf: $T $(PDF)


$(BINDIR):
	mkdir -p $(BINDIR)


help:
	$(call dump,C)
	$(call dump,CPP)
	$(call dump,T)
	$(call dump,PDF)

$(EXE): $T

install: $(BINDIR)/$T | $(BINDIR)

$(BINDIR)/$T: $T | $(BINDIR)
	install $T $(BINDIR)
	

clean:
	rm -f $T $(EXE)
