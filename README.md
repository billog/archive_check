# archive_check

you will need **libarchive-dev**
It is built on the fabulous library: [libarchive](https://libarchive.org/)

## checks archives (e.g. .zip .tar .gz) for integrity and prints results

I actually knocked this up for cygwin when I had to do something on Windows.
It's almost impossible to check a zip file is OK on windows, so I did this.
When you download a whole bang of zip archives you can easily test them.

-d will delete failures, useful, but be careful.

NOTE:
An archive is a collection of files, not something like a single file gzipped.



        archive_check [-d char -v -u]

        checks archives (e.g. .zip .tar .gz) for integrity and prints results
        something like so:



    blah/blah/x.zip|PASS|<thread:0>
    blah/blah/y.zip|FAIL|<thread:0>


        -v      verbose
        -d      delete bad files (careful now!)
        -t      delimiter character
        -p      parallel processes (not much use, slower)

        RETURNS:
        returns 0 if no errors found. So if you have 10,000 files to check
        it will return an error unless ALL are a PASS




It has some stupid superfluous omp threading as an experiment.

## examples


    $ archive_check -v  bionicpup64-8.0-uefi.iso                               
    bionicpup64-8.0-uefi.iso|1|.
    bionicpup64-8.0-uefi.iso|2|Windows_Installer
    bionicpup64-8.0-uefi.iso|3|help
    bionicpup64-8.0-uefi.iso|4|boot.catalog
    bionicpup64-8.0-uefi.iso|5|efi.img
    bionicpup64-8.0-uefi.iso|6|isolinux.bin
    bionicpup64-8.0-uefi.iso|7|fix-usb.sh
    bionicpup64-8.0-uefi.iso|8|grub.cfg
    bionicpup64-8.0-uefi.iso|9|initrd.gz
    bionicpup64-8.0-uefi.iso|10|isolinux.cfg
    bionicpup64-8.0-uefi.iso|11|puppy_bionicpup64_8.0.sfs
    bionicpup64-8.0-uefi.iso|12|splash.png
    bionicpup64-8.0-uefi.iso|13|vesamenu.c32
    bionicpup64-8.0-uefi.iso|14|vmlinuz
    bionicpup64-8.0-uefi.iso|15|zdrv_bionicpup64_8.0.sfs
    bionicpup64-8.0-uefi.iso|16|Windows_Installer/LICK-1.3.1-win32.zip
    bionicpup64-8.0-uefi.iso|17|Windows_Installer/README.txt
    bionicpup64-8.0-uefi.iso|18|help/help.msg
    bionicpup64-8.0-uefi.iso|19|help/help2.msg
    bionicpup64-8.0-uefi.iso|19 entries
    bionicpup64-8.0-uefi.iso|0 issues
    bionicpup64-8.0-uefi.iso|PASS|<thread:0>
    0 from 1 failed, TOTAL SUCCESS!

