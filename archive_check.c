#include <stdio.h>
#include <stdlib.h>
#include <archive_entry.h>
#include <archive.h>
#include <unistd.h>
#include <omp.h>


/*
 *
 * Attempt to check an archive (specifically zip file)
 * Should work for all archive types.
 * Will fail on a regular file so be careful wit the -d
 *
 */

static int verbose;
static int delete;
static int delimiter = '|';
static char * progname;

static int usage(char const * message)
{
	fprintf(stderr, "\n\n\t%s [-d char -v -u]\n\n", progname);

const char * m =
"	checks archives (e.g. .zip .tar .gz) for integrity and prints results\n\
	something like so:\n\
\n\
\n\
\n\
blah/blah/x.zip|PASS|<thread:0>\n\
blah/blah/y.zip|FAIL|<thread:0>\n\
\n\
\n\
	-v	verbose\n\
	-d	delete bad files (careful now!)\n\
	-t	delimiter character\n\
	-p	parallel processes (not much use, slower)\n\
\n\
	RETURNS:\n\
	returns 0 if no errors found. So if you have 10,000 files to check\n\
	it will return an error unless ALL are a PASS\n\
\n" ;

	fprintf(stderr, "%s", m);
	return 1;
}


static int delete_file(char const * path)
{
	if (path == NULL) return 0;
	printf("Deleting:%s\n", path);
	if (unlink(path) == -1) {
		perror(path);
	}
	return 1;
}

static void print_error(char const * path, struct archive * a)
{
	char const * msg = archive_error_string(a);
	msg = msg ? msg : "unknown" ;
	fprintf(stderr, "Oops! %s:%s\n", path, msg);
}

// returns number of issues found, so zero is GOOD :-)
static int archive_issues(char const * path, struct archive * a)
{
	int issues = 0;
	int count = 0;
	struct archive_entry *entry;

	for(;;) {
		switch (archive_read_next_header(a, &entry)) {
			case ARCHIVE_FATAL:
				issues++;
				print_error(path, a);
				goto END;

			case ARCHIVE_OK:
				archive_read_data_skip(a);  // Note 2
				count++;
				if(verbose)
					printf("%s%c%d%c%s\n", path, delimiter, count, delimiter,
								archive_entry_pathname(entry));
				break;

			case ARCHIVE_EOF:
				goto END;

			default:
			print_error(path, a);
			issues++;
		}
	}

END:
	return issues;

}


static int archive_ok(char const * path)
{

	int r;
	int issues = 0;

	struct archive *a = NULL;
	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);
	r = archive_read_open_filename(a, path, 10240); // Note 1

	if (r != ARCHIVE_OK) {
		print_error(path, a);
		issues++;

	} else {
		issues = archive_issues( path, a);
	}

	if(verbose) printf("%s%c%d entries\n", path, delimiter, archive_file_count(a));
	if(verbose) printf("%s%c%d issues\n", path, delimiter, issues );
	archive_read_free(a);  // Note 3
	return issues == 0;
}

int main(int argc, char ** argv)
{
	int errors = 0;
	int ch;
	int n;
	char * result = "unknown";
	int nthreads = 1;

	progname = *argv;

	while( (ch = getopt(argc, argv, "vdt:hp:")) != -1)  {
		switch (ch) {
			case 'v': verbose++; break;
			case 'd': delete++; break;
			case 't': delimiter = *optarg;break;
			case 'p': nthreads = atoi(optarg);break;
			case 'h':
			default: return usage("oops");
		}
	}

	if(nthreads > 0) omp_set_num_threads(nthreads);

	
	#pragma omp parallel for
	for ( n = optind; n < argc; ++n) {

		int thread = omp_get_thread_num();

		if (archive_ok( argv[n])) {
			result = "PASS";
		} else {
			++errors;
			result = "FAIL";
			if(delete) delete_file(argv[n]);
		}

		printf("%s%c%s%c<thread:%d>\n", argv[n], delimiter, result, delimiter, thread);
	}
	printf("%d from %d failed%s\n", errors, argc-optind,
							(errors? " (use -d to delete)":", TOTAL SUCCESS!"));


	return errors > 0;
	
}
/*
 * from : 

Note 1: Beginning with libarchive 3.0, the `archive_read_open_filename()`
function inspects the file before deciding how to handle the blocksize. If
the filename provided refers to a tape device, for example, it will use
exactly the blocksize you specify. For other devices, it may adjust the
requested blocksize in order to obtain better performance.

Note 2: The call to `archive_read_data_skip()` here is not actually
necessary, since libarchive will invoke it automatically if you request
the next header without reading the data for the last entry.

Note 3: This function was called `archive_read_finish()` in earlier
versions of libarchive. That name will remain available for at least a
few years.

*/
